'use strict';

var mongoose = require('mongoose'),
  chalk = require('chalk'),
  config = require('../config/config'),
  mg = require('../config/lib/mongoose'),
  async = require('async'),
  commandLineArgs = require('command-line-args'),
  cmd = require('node-cmd'),
  _ = require('lodash');

async.waterfall([
  // connect to db
  function (done) {
    mg.loadModels();

    mg.connect(function (db) {
      done(null, db);
    });
  },
  // find all apps
  function (db, done) {
    var Application = mongoose.model('Application');

    console.log('checking non-working servers.....');
    Application.find().exec(function (err, apps) {
      if (err) {
        throw err;
      }

      if (apps.length === 0) {
        console.log('nothing resumed, 0 Applications');
        return;
      }

      done(null, apps);
    });
  },
  // check non-working apps
  function (apps, done) {
    var processedCount = 0;
    var filtered = [];
    apps.forEach(function (app, index, arr) {
      cmd.get('node ./scripts/goon.js check --name ' + app, function (err, data, stderr) {
        console.log('check app - ' + app.appid + '  ', data);
        if (data.indexOf('running') < 0) filtered.push(app);
        if (++processedCount === arr.length) {
          done(null, filtered);
        }
      });
    });
  },
  // running servers for non-working app
  function (apps, done) {
    var processedCount = 0;
    console.log('non-working servers are all of ' + apps.length);
    apps.forEach(function (app, index, arr) {
      var cmdString = 'node ./scripts/goon.js start '
        + ' --name ' + app.appid
        + ' --port ' + app.server_port
        + ' --path ' + config.rootStoragePath + '/' + app.appid
        + ' --adminToken ' + app.appkey;
      _.forEach(app.server_env || {}, function (value, key) {
        cmdString += ' -e ' + key + '=' + value;
      });
      console.log(cmdString);
      cmd.get(cmdString, function (err, data, stderr) {
        if (!err) {
          console.log('launch server for app -' + app.appid);
        }
        if (++processedCount === arr.length) {
          console.log('all done');
          done();
        }
      });
    });
  }
]);
