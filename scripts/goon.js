'use strict';

var mongoose = require('mongoose'),
  async = require('async'),
  commandLineArgs = require('command-line-args'),
  cmd = require('node-cmd');

function doCommand() {
  const mainOptions = commandLineArgs([{ name: 'command', defaultOption: true }], { stopAtFirstUnknown: true });
  const argv = mainOptions._unknown || [];

  if (mainOptions.command === 'start') {
    doCommandRun(argv);
  } else if (mainOptions.command === 'check') {
    doCommandCheck(argv);
  } else if (mainOptions.command === 'stop') {
    doCommandStop(argv);
  }
}

function doCommandRun(argv) {
  class EnvType {
    constructor(env) {
      var envs = env.split('=');
      this.key = envs[0];
      this.value = envs[1];
    }
  }
  const optionDefinitions = [
    { name: 'name', type: String },
    { name: 'port', alias: 'p', type: Number },
    { name: 'path', type: String },
    { name: 'adminToken', type: String },
    { name: 'env', alias: 'e', type: env => new EnvType(env), multiple: true }
  ];

  var options = commandLineArgs(optionDefinitions, { argv: argv, partial: true });

  async.waterfall([
    // launch mongodb server
    function (done) {
      console.log(options);
      var db_container = 'gc_' + options.name + '_db';
      var port = options.port + 4;
      var exposePort = '-p ' + port + ':27017 ';
      // sudo -i \
      cmd.get(`
        docker run -d \
        --name ` + db_container + ` \
        ` + exposePort + ` \
        -v ` + options.path + `/db:/data/db \
        --restart always \
        mongo:latest
      `, function (err, data, stderr) {
        console.log(err, data, stderr);
        if (!err) {
          console.log(data);
          done(null, db_container);
        } else {
          console.log('err - db server failed');
          doCommandStop(argv, true);
        }
      });
    },
    // launch chat server
    function (db_container, done) {
      var port = options.port;
      var adminToken = '-e LCB_ADMIN_TOKEN=' + options.adminToken + ' ';
      var lcbPass = options.adminToken ? adminToken : '';
      var env = options.env || [];
      env.forEach(function (item, index) {
        lcbPass += ' -e LCB_' + item.key.toUpperCase() + '=' + item.value;
      });
      // sudo -i \
      var command = `
        docker run -d \
        --link ` + db_container + `:mongo \
        ` + lcbPass + ` \
        -e LCB_DATABASE_URI=mongodb://mongo/goon_chat \
        -v ` + options.path + `/uploads:/var/www/uploads \
        -p ` + port++ + `:5000 \
        -p ` + port++ + `:5001 \
        -p ` + port++ + `:5222 \
        --name gc_` + options.name + `_server\
        --restart always \
        covernal/goon_chat:latest \
        npm start --prefix /var/www
      `;
      console.log(command);
      cmd.get(command, function (err, data, stderr) {
        if (!err) {
          console.log(data);
          done();
        } else {
          console.log('err - chat server failed');
          console.log(err);
          console.log(stderr);
          doCommandStop(argv, true);
        }
      });
    }
  ]);
}

function doCommandCheck(argv) {
  const optionDefinitions = [
    { name: 'name', type: String }
  ];

  var options = commandLineArgs(optionDefinitions, { argv: argv, partial: true });

  cmd.run('docker ps | grep gc_' + options.name + '_', function (err, data, stderr) {
    if (!err) {
      data = data || '';
      var _prefix = 'gc_' + options.name + '_';
      if (data.indexOf(_prefix + 'server') >= 0 && data.indexOf(_prefix + 'db') >= 0) {
        console.log('running');
      } else {
        console.log('not found');
      }
    } else {
      console.log('not found');
    }
  });
}

function doCommandStop(argv, silent) {
  const optionDefinitions = [
    { name: 'name', type: String },
    { name: 'all', alias: 'a', type: Boolean }
  ];

  var options = commandLineArgs(optionDefinitions, { argv: argv, partial: true });

  if (options.all) {
    return doCommandStopAll();
  }

  var _prefix = 'gc_' + options.name + '_';
  var db_server = _prefix + 'db';
  var chat_server = _prefix + 'server';
  cmd.get('docker rm -f ' + db_server + ' ' + chat_server, function (err, data, stderr) {
    if (!err) {
      if (!silent) console.log('stopped');
    } else {
      if (!silent) console.log('err');
      // console.log(err, data, stderr);
    }
  });
}

function doCommandStopAll() {
  cmd.get('docker rm -f $(docker ps -a -q -f name=^/gc_)', function (err, data, stderr) {
    if (!err) {
      console.log('stopped all');
    } else {
      console.log('err');
    }
  });
}
doCommand();
