(function () {
  'use strict';

  // Configuring the Articles Admin module
  angular
    .module('applications')
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(Menus) {
    Menus.addMenuItem('topbar', {
      title: 'Applications',
      state: 'applications.list'// ,
      // type: 'dropdown',
      // roles: ['*']
    });
  }
}());
