(function () {
  'use strict';

  angular
    .module('applications')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('applications', {
        abstract: true,
        url: '/applications',
        template: '<ui-view/>'
      })
      .state('applications.list', {
        url: '',
        templateUrl: '/modules/applications/client/views/list-applications.client.view.html',
        controller: 'ApplicationsListController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Applications List'
        }
      })
      .state('applications.create', {
        url: '/create',
        templateUrl: '/modules/applications/client/views/form-application.client.view.html',
        controller: 'ApplicationsController',
        controllerAs: 'vm',
        resolve: {
          applicationResolve: newApplication
        },
        data: {
          roles: ['user', 'admin'],
          pageTitle: 'Applications Create'
        }
      })
      .state('applications.view', {
        abstract: true,
        url: '/v/:applicationId',
        templateUrl: '/modules/applications/client/views/view-app.client.sidebar.html',
        controller: 'ApplicationsSidebarController',
        controllerAs: 'vm',
        resolve: {
          applicationResolve: getApplication
        },
        data: {
          roles: ['user', 'admin']
        }
      })
      .state('applications.view.edit', {
        url: '/edit',
        templateUrl: '/modules/applications/client/views/view-application.client.view.html',
        controller: 'ApplicationsController',
        controllerAs: 'vm',
        resolve: {
          applicationResolve: getApplication
        },
        data: {
          pageTitle: 'Application {{ applicationResolve.name }}'
        }
      })
      .state('applications.view.users', {
        url: '/users',
        templateUrl: '/modules/applications/client/views/list-users.client.view.html',
        controller: 'ApplicationsUserListController',
        controllerAs: 'vm',
        resolve: {
          applicationResolve: getApplication
        },
        data: {
          pageTitle: 'Application {{ applicationResolve.name }}'
        }
      })
      .state('applications.view.locations', {
        url: '/locations',
        templateUrl: '/modules/applications/client/views/list-locations.client.view.html',
        controller: 'ApplicationsLocationListController',
        controllerAs: 'vm',
        resolve: {
          applicationResolve: getApplication
        },
        data: {
          pageTitle: 'Application {{ applicationResolve.name }}'
        }
      })
      .state('applications.view.push', {
        url: '/push',
        templateUrl: '/modules/applications/client/views/list-push.client.view.html',
        controller: 'ApplicationsPushListController',
        controllerAs: 'vm',
        resolve: {
          applicationResolve: getApplication
        },
        data: {
          pageTitle: 'Application {{ applicationResolve.name }}'
        }
      });
  }

  getApplication.$inject = ['$stateParams', 'ApplicationsService'];

  function getApplication($stateParams, ApplicationsService) {
    return ApplicationsService.get({
      applicationId: $stateParams.applicationId
    }).$promise;
  }

  newApplication.$inject = ['ApplicationsService'];

  function newApplication(ApplicationsService) {
    return new ApplicationsService();
  }
}());
