(function () {
  'use strict';

  // Applications Sidebar controller
  angular
    .module('applications')
    .controller('ApplicationsSidebarController', ApplicationsSidebarController);

  ApplicationsSidebarController.$inject = ['$scope', '$state', '$window', 'Authentication', 'applicationResolve', 'ApplicationsService', 'Notification', 'menuService', 'Upload', '$timeout'];

  function ApplicationsSidebarController($scope, $state, $window, Authentication, application, ApplicationService, Notification, menuService, Upload, $timeout) {
    var vm = this;
    var appID = null;

    vm.authentication = Authentication;
    vm.application = application;
    vm.application.profileImageURL = vm.application.profileImageURL ? vm.application.profileImageURL : '/modules/core/client/img/ic-setting-white.png';

    function buildSideMenu() {
      console.log('buildSideMenu');
      menuService.addMenu('sidebar', {
        roles: ['user']
      });

      if (menuService.menus.sidebar.items.application) menuService.removeMenuItem('sidebar', 'application');

      menuService.addMenuItem('sidebar', {
        title: 'Application',
        state: 'application',
        type: 'dropdown',
        roles: ['*']
      });
      menuService.addSubMenuItem('sidebar', 'application', {
        title: 'Overview',
        state: 'applications.view.edit',
        icon: 'glyphicon glyphicon-th-large'
      });
      menuService.addSubMenuItem('sidebar', 'application', {
        title: 'Users',
        state: 'applications.view.users',
        icon: 'glyphicon glyphicon-user'
      });
      menuService.addSubMenuItem('sidebar', 'application', {
        title: 'Locations',
        state: 'applications.view.locations',
        icon: 'glyphicon glyphicon-screenshot'
      });
      // menuService.addSubMenuItem('sidebar', 'application', {
      //   title: 'Content',
      //   state: 'applications.view.media',
      //   icon: 'glyphicon glyphicon-file'
      // });
      menuService.addSubMenuItem('sidebar', 'application', {
        title: 'Push Notification',
        state: 'applications.view.push',
        icon: 'glyphicon glyphicon-bullhorn'
      });

      vm.sidemenu = menuService.menus.sidebar.items.filter(function (item) {
        return item.state === 'application';
      })[0];
      vm.sidemenu.items.forEach(function (item) {
        console.log(item.state, $state.current.name);
        if (item.state === $state.current.name) {
          item.active = true;
        }
      });
    }
    buildSideMenu();

    vm.go = function (item) {
      vm.sidemenu.items.forEach(function (it) {
        it.active = false;
      });
      item.active = true;
      $state.go(item.state, {
        applicationId: vm.application._id
      });
    };
  }
}());
