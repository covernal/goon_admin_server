(function () {
  'use strict';

  angular
    .module('applications')
    .controller('ApplicationsPushListController', ApplicationsPushListController);

  ApplicationsPushListController.$inject = ['$scope', 'ApplicationsService', 'Authentication', 'applicationResolve', '$timeout', 'Socket'];

  function ApplicationsPushListController($scope, ApplicationsService, Authentication, application, $timeout, Socket) {
    var vm = this;

    vm.authentication = Authentication;
    vm.application = application;
    vm.users = [];
    vm.application.server_env = vm.application.server_env || {};

    vm.isloading = false;
    vm.push = {};
    vm.test = {};

    vm.server_key = vm.application.server_env.fcm_server_key || '';

    vm.save = function () {
      vm.isloading = true;
      vm.processing_msg = 'Updating... (1/3)';

      vm.application.server_env.fcm_server_key = vm.server_key;

      var res = vm.application;
      var channel = 'update-' + res.appid;
      Socket.connect('broadcast');
      // Socket.emit('channel', channel);
      Socket.on('channel', function () {
        Socket.emit('channel', channel);
      });

      Socket.on('broadcast', function (message) {
        if (message === 'done') {
          $timeout(function () {
            vm.isloading = false;
          }, 400);
          vm.processing_msg = 'All Done! (3/3)';
        } else if (message === 'deleted') {
          // vm.isloading = false;
          vm.processing_msg = 'Re-launching... (2/3)';
        } else {
          vm.isloading = false;
          vm.error = 'Error orcurred. Please contact support team. Thanks.';
        }
      });

      // Remove the event listener when the controller instance is destroyed
      $scope.$on('$destroy', function () {
        Socket.removeListener('chatMessage');
      });

      vm.application.$update(successCallback, errorCallback);

      function successCallback(res) {
        vm.isloading = false;
      }

      function errorCallback(res) {
        vm.isloading = false;
      }

    };

    vm.onSendPush = function () {
      if (!!!vm.push.message) {
        return $('#testModal').modal('show');
      }

      vm.isloading = true;
      ApplicationsService.pushToAll(vm.application, vm.push.title, vm.push.message, function (data) {
        vm.isloading = false;
        console.log(data);
      });
    };

    vm.onSendTest = function () {
      if (!!!vm.test.token || !!!vm.test.message) {
        return $('#testModal').modal('show');
      }

      vm.isloading = true;
      ApplicationsService.pushToDevice(vm.application, vm.test.token, vm.test.title, vm.test.message, function (data) {
        vm.isloading = false;
        console.log(data);
      });
    };

    // vm.itemsPerPageChanged = function () {
    //   vm.currentPage = 1;
    //   vm.pageChanged();
    // };

    // vm.onSearch = function () {
    //   vm.currentPage = 1;
    //   vm.pageChanged();
    // };
  }
}());
