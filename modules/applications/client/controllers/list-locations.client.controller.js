(function () {
  'use strict';

  angular
    .module('applications')
    .controller('ApplicationsLocationListController', ApplicationsLocationListController);

  ApplicationsLocationListController.$inject = ['ApplicationsService', 'Authentication', 'applicationResolve', '$http'];

  function ApplicationsLocationListController(ApplicationsService, Authentication, application, $http) {
    var vm = this;

    vm.authentication = Authentication;
    vm.application = application;
    vm.users = [];
    vm.isloading = true;
    vm.itemsCounts = [10, 25, 50, 100];
    vm.itemsPerPage = vm.itemsCounts[0];
    vm.currentPage = 0;
    vm.search = undefined;

    vm.initMap = function () {
      vm.location = new google.maps.LatLng(0, 0);
      vm.mapCanvans = document.getElementById('map');
      var mapOptions = {
        center: vm.location,
        zoom: 1,
        panControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      vm.map = new google.maps.Map(vm.mapCanvans, mapOptions);
    };

    vm.addMarker = function (user) {
      if (vm.marker) {
        vm.marker.setMap(null);
      } else {
        vm.map.setZoom(10);
      }

      var latlng = user.geoLocation.split(/, ?/);
      var loc = new google.maps.LatLng(parseFloat(latlng[0]), parseFloat(latlng[1]));
      var marker = new google.maps.Marker({
        position: loc,
        map: vm.map
      });
      vm.map.panTo(loc);
      vm.marker = marker;

      var contentString = '<div class="info-window" style="color: black;"><div class="info-content"><h5>' + user.username + '</h5>' + user.email + '</div></div>';
      var infowindow = new google.maps.InfoWindow({
        content: contentString,
        maxWidth: 400
      });
      marker.addListener('click', function () {
        infowindow.open(vm.map, marker);
      });
    };

    ApplicationsService.getUsers(vm.application, 0, vm.itemsPerPage, vm.search, function (data) {
      console.log(data);
      vm.users = data.users || [];
      vm.isloading = false;
      vm.filterLength = data.count;
      vm.updateLocation();
    });

    vm.updateLocation = function () {
      vm.users.forEach(function (user) {
        user.geoLocation = '0, 0';
        $http.get('http://ipinfo.io/' + user.ipAddress + '/geo').
        success(function (data) {
          user.location = data.city ? data.city : '';
          user.location += user.location === '' ? '' : ', ';
          if (data.city !== data.region) {
            user.location += data.region ? data.region : '';
            user.location += user.location === '' ? '' : ', ';
          }
          user.location += data.country ? data.country : '';
          user.location += user.location === '' ? '-' : '';
          user.geoLocation = data.loc ? data.loc : '0, 0';
        });
      });
    };

    vm.pageChanged = function () {
      vm.isloading = true;
      ApplicationsService.getUsers(vm.application, (vm.currentPage - 1) * vm.itemsPerPage, vm.itemsPerPage, vm.search, function (data) {
        vm.users = data.users;
        vm.isloading = false;
        console.log(data);
        vm.filterLength = data.count;
      });
    };

    vm.itemsPerPageChanged = function () {
      vm.currentPage = 1;
      vm.pageChanged();
    };

    vm.onSearch = function () {
      vm.currentPage = 1;
      vm.pageChanged();
    };
  }
}());
