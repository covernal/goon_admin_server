(function () {
  'use strict';

  angular
    .module('applications')
    .controller('ApplicationsUserListController', ApplicationsUserListController);

  ApplicationsUserListController.$inject = ['ApplicationsService', 'Authentication', 'applicationResolve', 'Notification'];

  function ApplicationsUserListController(ApplicationsService, Authentication, application, Notification) {
    var vm = this;

    vm.authentication = Authentication;
    vm.application = application;
    vm.user = {
      username: '',
      email: '',
      password: '',
      passwordConfirm: '',
      firstName: '',
      lastName: ''
    };
    vm.users = [];
    vm.isloading = true;
    vm.itemsCounts = [10, 25, 50, 100];
    vm.itemsPerPage = vm.itemsCounts[0];
    vm.currentPage = 1;
    vm.search = undefined;

    ApplicationsService.getUsers(vm.application, 0, vm.itemsPerPage, vm.search, function (data) {
      vm.users = data.users;
      vm.isloading = false;
      console.log(data);
      vm.filterLength = data.count;
    });

    vm.pageChanged = function () {
      vm.isloading = true;
      ApplicationsService.getUsers(vm.application, (vm.currentPage - 1) * vm.itemsPerPage, vm.itemsPerPage, vm.search, function (data) {
        vm.users = data.users;
        vm.isloading = false;
        console.log(data);
        vm.filterLength = data.count;
      });
    };

    vm.itemsPerPageChanged = function () {
      vm.currentPage = 1;
      vm.pageChanged();
    };

    vm.onSearch = function () {
      vm.currentPage = 1;
      vm.pageChanged();
    };

    vm.showNewUserModal = function () {
      vm.user = {};
      $('#modalNewUser').modal('show');
    };

    vm.fullPath = function (url) {
      var path = ApplicationsService._chatserver_url(vm.application, '/' + url);
      console.log(path);
      return path;
    };

    vm.addNewUser = function (picFile) {
      ApplicationsService.addUser(vm.application, vm.user, function (data, err) {
        if (!err && data.status === 'success') {
          Notification.success({ message: 'New user added.' });
        } else if (err && err.status === 400) {
          return Notification.error({ message: err.data.message });
        }

        if (!picFile) {
          $('#modalNewUser').modal('hide');
          vm.fileSelected = false;
          return;
        }
        ApplicationsService.uploadAvatar(vm.application, data.user.id, picFile, function (data, err) {
          vm.pageChanged();
          $('#modalNewUser').modal('hide');
          vm.fileSelected = false;
        });
      });
    };

    vm.editUser = function (user) {
      console.log(user);
      vm.user0 = user;
      vm.user = $.extend({}, user);
      $('#modalEditUser').modal('show');
    };

    vm.saveUser = function (old, updated, picFile) {
      var data = {
        id: updated.id
      };

      if (old.email !== updated.email) data.email = updated.email;
      if (old.username !== updated.username) data.username = updated.username;
      if (old.firstName !== updated.firstName) data.firstName = updated.firstName;
      if (old.lastName !== updated.lastName) data.lastName = updated.lastName;

      ApplicationsService.updateUser(vm.application, data, function (data, err) {
        console.log(data, err);
        if (!err && data.status === 'success') {
          Notification.success({ message: 'User saved.' });
        } else if (err && err.status === 400) {
          return Notification.error({ message: err.data.message });
        }
        // vm.pageChanged();
        old.email = updated.email;
        old.username = updated.username;
        old.firstName = updated.firstName;
        old.lastName = updated.lastName;

        if (!picFile) {
          $('#modalEditUser').modal('hide');
          vm.fileSelected = false;
          return;
        }

        ApplicationsService.uploadAvatar(vm.application, updated.id, picFile, function (data, err) {
          if (data) old.avatar = data.avatar;
          $('#modalEditUser').modal('hide');
          vm.fileSelected = false;
        });
      });
    };
  }
}());
