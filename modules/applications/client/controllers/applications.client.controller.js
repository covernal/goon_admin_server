(function () {
  'use strict';

  // Applications controller
  angular
    .module('applications')
    .controller('ApplicationsController', ApplicationsController);

  ApplicationsController.$inject = ['$scope', '$state', '$window', 'Authentication', 'applicationResolve', 'ApplicationsService', 'Notification', 'menuService', 'Upload', '$timeout', 'Socket'];

  function ApplicationsController($scope, $state, $window, Authentication, application, ApplicationService, Notification, menuService, Upload, $timeout, Socket) {
    var vm = this;
    var appID = null;

    vm.authentication = Authentication;
    vm.application = application;
    vm.application.server_env = vm.application.server_env || {};
    vm.error = null;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;
    vm.create = create;
    vm.delete = remove;
    vm.buildNewAppID = buildNewAppID;
    vm.buildNewAppKey = buildNewAppKey;
    vm.copyFrom = copyFrom;
    vm.application.profileImageURL = vm.application.profileImageURL ? vm.application.profileImageURL : '/modules/core/client/img/ic-setting-white.png';
    vm.processing_msg = '';

    vm.isloading = false;
    vm.progress = 0;
    vm.upload = function (dataUrl) {

      Upload.upload({
        url: '/api/applications/' + vm.application._id + '/picture',
        data: {
          newProfilePicture: dataUrl
        }
      }).then(function (response) {
        $timeout(function () {
          onSuccessItem(response.data);
        });
      }, function (response) {
        if (response.status > 0) onErrorItem(response.data);
      }, function (evt) {
        vm.progress = parseInt(100.0 * evt.loaded / evt.total, 10);
      });
    };

    // Called after the user has successfully uploaded a new picture
    function onSuccessItem(response) {
      // Show success message
      Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> Successfully changed profile picture' });

      // Populate user object
      vm.application = response;

      // Reset form
      vm.fileSelected = false;
      vm.progress = 0;
      vm.loaded = false;
    }

    // Called after the user has failed to upload a new picture
    function onErrorItem(response) {
      vm.fileSelected = false;
      vm.progress = 0;

      // Show error message
      Notification.error({ message: response.message, title: '<i class="glyphicon glyphicon-remove"></i> Failed to change profile picture' });
    }

    // build new app id
    function buildNewAppID() {
      if (appID !== vm.application.name) {
        appID = vm.application.name;
        ApplicationService.buildAppid(appID, true)
          .then(function (response) {
            vm.application.appid = response.appid || '';
          })
          .catch(function (response) {
            Notification.error({ message: 'Please set another App ID', title: '<i class="glyphicon glyphicon-remove"></i> AppID alreay exists', delay: 4000 });
          });
      }
    }

    function copyFrom(containerid) {
      var range;
      if (document.selection) {
        range = document.body.createTextRange();
        range.moveToElementText(document.getElementById(containerid));
        range.select().createTextRange();
        document.execCommand('copy');
      } else if (window.getSelection) {
        range = document.createRange();
        range.selectNode(document.getElementById(containerid));
        window.getSelection().removeAllRanges(range);
        window.getSelection().addRange(range);
        document.execCommand('copy');
      }
    }

    // build new app key
    function buildNewAppKey() {
      vm.application.$buildNewAppKey(function (response) {
        console.log(response);
      }, function (response) {
        Notification.error({ message: 'Please try again later', title: '<i class="glyphicon glyphicon-remove"></i> Failure', delay: 4000 });
      });
    }

    // Remove existing Application
    function remove() {
      vm.isloading = true;
      vm.processing_msg = 'Deleting...';
      
      vm.application.$remove(function (response) {
        $timeout(function () {
          vm.isloading = false;
          $state.go('applications.list');
        }, 300);
      });
    }

    // Save Application
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.applicationForm');
        return false;
      }

      // TODO: move create/update logic to service
      if (vm.application._id) {
        var res = vm.application;
        var channel = 'update-' + res.appid;
        Socket.connect('broadcast');
        // Socket.emit('channel', channel);
        Socket.on('channel', function () {
          Socket.emit('channel', channel);
        });

        Socket.on('broadcast', function (message) {
          if (message === 'done') {
            $timeout(function () {
              vm.isloading = false;
            }, 400);
            vm.processing_msg = 'All Done! (3/3)';
            $state.go('applications.view.edit', {
              applicationId: res._id
            });
          } else if (message === 'deleted') {
            // vm.isloading = false;
            vm.processing_msg = 'Re-launching... (2/3)';
            $state.go('applications.view.edit', {
              applicationId: res._id
            });
          } else {
            vm.isloading = false;
            vm.error = 'Error orcurred. Please contact support team. Thanks.';
          }
        });

        // Remove the event listener when the controller instance is destroyed
        $scope.$on('$destroy', function () {
          Socket.removeListener('chatMessage');
        });

        vm.application.$update(successCallback, errorCallback);
      } else {
        vm.application.$save(successCallback, errorCallback);
      }

      vm.isloading = true;
      vm.processing_msg = 'Updating... (1/3)';

      function successCallback(res) {
        $state.go('applications.view', {
          applicationId: res._id
        });
      }

      function errorCallback(res) {
        vm.error = res.data.message;
      }
    }

    function create() {
      if (!!!vm.application.name) {
        return $('#modalEmptyAppName').modal('show');
      }

      vm.application.server_env.fcm_server_key = vm.fcm;
      vm.isloading = true;
      vm.processing_msg = 'Creating... (1/2)';
      vm.application.$save(successCallback, errorCallback);
      function successCallback(res) {
        vm.processing_msg = 'Launching... (2/2)';

        var channel = 'create-' + res.appid;
        Socket.connect('broadcast');
        // Socket.emit('channel', channel);
        Socket.on('channel', function () {
          Socket.emit('channel', channel);
        });

        Socket.on('broadcast', function (message) {
          if (message === 'done') {
            // vm.isloading = false;
            vm.processing_msg = 'All Done! (2/2)';
            $state.go('applications.view.edit', {
              applicationId: res._id
            });
          } else {
            vm.isloading = false;
            vm.error = 'Error orcurred. Please contact support team. Thanks.';
          }
        });

        // Socket.emit('chatMessage', {
        //   text: 'test msg'
        // });

        // Remove the event listener when the controller instance is destroyed
        $scope.$on('$destroy', function () {
          Socket.removeListener('chatMessage');
        });
      }

      function errorCallback(res) {
        vm.isloading = false;
        vm.error = res.data.message;
      }
    }
  }
}());
