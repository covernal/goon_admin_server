// Applications service used to communicate Applications REST endpoints
(function () {
  'use strict';

  angular
    .module('applications')
    .factory('ApplicationsService', ApplicationsService);

  ApplicationsService.$inject = ['$resource', '$http', 'Upload'];

  function ApplicationsService($resource, $http, Upload) {
    var Applications = $resource('/api/applications/:applicationId', {
      applicationId: '@_id'
    }, {
      update: {
        method: 'PUT'
      },
      getNewAppid: {
        method: 'GET',
        url: '/api/applications/getid/:hint/:auto?'
      },
      buildNewAppKey: {
        method: 'PUT',
        url: '/api/applications/:applicationId/newkey'
      }
    });

    angular.extend(Applications, {
      buildAppid: function (hint, auto) {
        return this.getNewAppid({
          hint: hint,
          auto: auto
        }).$promise;
      },
      _chatserver_url: function (app, suburl) {
        var url = 'http://' + app.server_host + ':' + app.server_port + suburl;
        console.log(url);
        return url;
      },
      _connect: function (app, cb) {
        app.chat_token = app.appkey;
        if (app.chat_token !== undefined) {
          if (cb) cb(null);
          return;
        }
        // var thiz = this;
        // $http.post(
        //   thiz._chatserver_url(app, '/login'),
        //   {
        //     email: app.admin_account,
        //     password: app.admin_password,
        //     resource: 'dashboard'
        //   }
        // ).then(function (response) {
        //   app.chat_token = response.data.token;
        //   if (cb) cb(null);
        // }, function (err) {
        //   if (cb) cb(err);
        // });
      },
      addUser: function (app, user, cb) {
        var thiz = this;
        this._connect(app, function (err) {
          if (err) return cb(err);
          $http.post(
            thiz._chatserver_url(app, '/signup'),
            user
          ).then(function (response) {
            if (cb) cb(response.data);
          }, function (err) {
            if (cb) cb(null, err);
          });
        });
      },
      updateUser: function (app, user, cb) {
        var thiz = this;
        this._connect(app, function (err) {
          if (err) return cb(err);
          $http.post(
            thiz._chatserver_url(app, '/api/users/edit/' + user.id),
            user,
            {
              headers: {
                'auth-token': app.chat_token
              }
            }
          ).then(function (response) {
            if (cb) cb(response.data);
          }, function (err) {
            if (cb) cb(null, err);
          });
        });
      },
      uploadAvatar: function (app, user_id, file, cb) {
        var thiz = this;
        this._connect(app, function (err) {
          if (err) return cb(err);
          var fd = new FormData();
          fd.append('file', file);
          $http.post(
            thiz._chatserver_url(app, '/api/users/avatar/' + user_id),
            fd,
            {
              headers: {
                'auth-token': app.chat_token,
                'Content-Type': undefined
              }
            }
          ).then(function (response) {
            cb(response.data);
          }, function (err) {
            cb(null, err);
          });
        });
      },
      getUsers: function (app, start, length, pattern, cb) {
        var thiz = this;
        this._connect(app, function (err) {
          if (err) return cb(err);
          $http.get(
            thiz._chatserver_url(app, '/api/users/all/' + start + '/' + length + (pattern ? '/' + pattern : '')),
            {
              headers: {
                'auth-token': app.chat_token
              }
            }
          ).then(function (response) {
            if (cb) cb(response.data);
          }, function (err) {
            if (cb) cb(null, err);
          });
        });
      },
      pushToDevice: function (app, token, title, message, cb) {
        var thiz = this;
        this._connect(app, function (err) {
          if (err) return cb(err);
          $http.post(
            thiz._chatserver_url(app, '/api/push/device'),
            {
              token: token,
              title: title,
              message: message
            },
            {
              headers: {
                'auth-token': app.chat_token
              }
            }
          ).then(function (response) {
            if (cb) cb(true);
          }, function (err) {
            if (cb) cb(false, err);
          });
        });
      },
      pushToAll: function (app, title, message, cb) {
        var thiz = this;
        this._connect(app, function (err) {
          if (err) return cb(err);
          $http.post(
            thiz._chatserver_url(app, '/api/push/all'),
            {
              title: title,
              message: message
            },
            {
              headers: {
                'auth-token': app.chat_token
              }
            }
          ).then(function (response) {
            if (cb) cb(true);
          }, function (err) {
            if (cb) cb(false, err);
          });
        });
      }
    });

    return Applications;
  }
}());
