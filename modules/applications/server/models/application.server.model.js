'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  crypto = require('crypto'),
  Schema = mongoose.Schema;

var validateAppID = function (appid) {
  var appidRegex = /^(?=[\w.-]+$)(?!.*[._-]{2})(?!\.)(?!.*\.$).{3,34}$/;
  return (
    appid && appidRegex.test(appid)
  );
};

/**
 * Application Schema
 */
var ApplicationSchema = new Schema({
  name: {
    type: String,
    default: '',
    required: 'Please fill Application name',
    trim: true
  },
  created: {
    type: Date,
    default: Date.now
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  appid: {
    type: String,
    index: {
      unique: 'AppID already exists',
      sparse: true // For this to work on a previously indexed field, the index must be dropped & the application restarted.
    },
    required: 'Please fill in a AppID',
    validate: [validateAppID, 'Please enter a valid appid: 3+ characters long, no consecutive dots, does not begin or end with dots, letters a-z and numbers 0-9.'],
    lowercase: true,
    trim: true
  },
  appkey: {
    type: String
  },
  profileImageURL: {
    type: String,
    default: '/modules/core/client/img/ic-setting-white.png'
  },
  description: {
    type: String
  },
  status: {
    type: String
  },
  server_host: {
    type: String
  },
  server_port: {
    type: Number
  },
  server_env: Schema.Types.Mixed
});

// /**
//  * Hook a pre save method to hash the password
//  */
// ApplicationSchema.pre('save', function (next) {

//   if (!!!this.appkey) {
//     this.appkey = this.generateAppKey();
//     this.markModified('appkey');
//   }

//   next();
// });

/**
 * Create instance method for random api key
 */
ApplicationSchema.methods.generateAppKey = function () {
  this.appkey = crypto.randomBytes(10).toString('hex')
    + '-' + crypto.randomBytes(2).toString('hex')
    + '-' + crypto.randomBytes(2).toString('hex')
    + '-' + crypto.randomBytes(2).toString('hex');
  this.markModified('appkey');
};

mongoose.model('Application', ApplicationSchema);
