'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Application = mongoose.model('Application'),
  async = require('async'),
  crypto = require('crypto'),
  cmd = require('node-cmd'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  fs = require('fs'),
  multer = require('multer'),
  multerS3 = require('multer-s3'),
  aws = require('aws-sdk'),
  amazonS3URI = require('amazon-s3-uri'),
  config = require(path.resolve('./config/config')),
  _ = require('lodash');


var useS3Storage = config.uploads.storage === 's3' && config.aws.s3;
var s3;

var _buildAppID = function (hint, auto, cb) {
  hint = hint.replace(/[^a-zA-Z0-9]+/g, '-').toLowerCase();
  var generated = '';

  var generateOrCheckAppID = function () {
    if (auto) {
      generated = hint + '-' + crypto.randomBytes(6).toString('hex');
    } else {
      generated = hint;
    }

    Application.findOne({ 'appid': generated }).exec(function (err, application) {
      if (err) {
        return cb(undefined, err);
      } else if (!application) {
        return cb(generated);
      } else if (application) {
        if (auto) {
          generateOrCheckAppID();
        } else {
          cb(undefined, 'AppID is not unique. Please use another.');
        }
      }
    });
  };

  generateOrCheckAppID();
};

var _buildServerPort = function (cb) {
  var port = 30000;
  var bandwidth = 5;

  var checkServerPort = function () {
    Application.findOne({ 'server_port': port }).exec(function (err, application) {
      if (err) {
        return cb(undefined, err);
      } else if (!application) {
        return cb(port);
      } else if (application) {
        port += bandwidth;
        checkServerPort();
      }
    });
  };

  checkServerPort();
};

/**
 * Create a Application
 */
exports.create = function (req, res) {
  var application = new Application(req.body);
  application.user = req.user;
  application.generateAppKey();

  application.server_host = req.hostname;

  async.waterfall([
    function (done) {
      _buildAppID(application.name, true, function (appid, err) {
        console.log('app id has generated', appid, err);
        if (err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        } else {
          application.appid = appid;
          done();
        }
      });
    },
    function (done) {
      _buildServerPort(function (port, err) {
        console.log('app id has generated', port, err);
        if (err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        } else {
          application.server_port = port;
          done();
        }
      });
    },
    function (done) {
      application.save(function (err) {
        if (err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err), app: application
          });
        } else {
          res.jsonp(application);
        }
      });

      done(null, application);
    },
    function (application, done) {
      var app = req.app;
      var cmdString = 'node ./scripts/goon.js start '
        + ' --name ' + application.appid
        + ' --port ' + application.server_port
        + ' --path ' + config.rootStoragePath + '/' + application.appid
        + ' --adminToken ' + application.appkey;
      console.log(cmdString);
      cmd.get(cmdString, function (err, data, stderr) {
        // console.log('done~~~~~');
        // console.log(err, data, stderr);
        if (!err) {
          app.emit('broadcast', {
            channel: 'create-' + application.appid,
            body: 'done'
          });
        } else {
          app.emit('broadcast', {
            channel: 'create-' + application.appid,
            body: 'done'
          });
        }
      });
    }
  ]);
};

/**
 * Show the current Application
 */
exports.read = function (req, res) {
  // convert mongoose document to JSON
  var application = req.application ? req.application.toJSON() : {};

  // Add a custom field to the Article, for determining if the current User is the "owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Article model.
  application.isCurrentUserOwner = req.user && application.user && application.user._id.toString() === req.user._id.toString();

  res.jsonp(application);
};

/**
 * Update a Application
 */
exports.update = function (req, res) {
  console.log('updated server info');

  var application = req.application;

  application = _.extend(application, req.body);

  async.waterfall([
    function (done) {
      application.save(function (err) {
        if (err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        } else {
          res.jsonp(application);
          done(null, application);
        }
      });
    },
    function (application, done) {
      console.log('stopping server');
      var app = req.app;
      var cmdString = 'node ./scripts/goon.js stop --name ' + application.appid;
      console.log(cmdString);
      cmd.get(cmdString, function (err, data, stderr) {
        console.log(err, data, stderr);
        app.emit('broadcast', {
          channel: 'update-' + application.appid,
          body: 'deleted'
        });
        done(null, application);
      });
    },
    function (application, done) {
      console.log('starting server');
      var app = req.app;
      var cmdString = 'node ./scripts/goon.js start '
        + ' --name ' + application.appid
        + ' --port ' + application.server_port
        + ' --path ' + config.rootStoragePath + '/' + application.appid
        + ' --adminToken ' + application.appkey;
      _.forEach(application.server_env || {}, function (value, key) {
        cmdString += ' -e ' + key + '=' + value;
      });
      console.log(cmdString);
      cmd.get(cmdString, function (err, data, stderr) {
        // console.log('done~~~~~');
        // console.log(err, data, stderr);
        if (!err) {
          app.emit('broadcast', {
            channel: 'update-' + application.appid,
            body: 'done'
          });
        } else {
          app.emit('broadcast', {
            channel: 'update-' + application.appid,
            body: 'done'
          });
        }
      });
    }
  ]);
};

/**
 * Delete an Application
 */
exports.delete = function (req, res) {
  var application = req.application;

  application.remove(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      var app = req.app;
      var cmdString = 'node ./scripts/goon.js stop --name ' + application.appid;
      console.log(cmdString);
      cmd.get(cmdString, function (err, data, stderr) {
        // console.log('done~~~~~');
        console.log(err, data, stderr);
        res.jsonp(application);
      });
    }
  });
};

/**
 * List of Applications
 */
exports.list = function (req, res) {
  // sort({name: 1}).
  Application.find({ 'user': req.user.id }).sort('+created').populate('user', 'displayName').exec(function (err, applications) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(applications);
    }
  });
};

/**
 * Build a new Application Key
 */
exports.buildNewKey = function (req, res) {
  var application = req.application;
  application.generateAppKey();

  application.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(application);
    }
  });
};


/**
 * Build uniqu app id from hint string
 */
exports.getNewAppID = function (req, res) {
  var hint = req.params.hint;
  var auto = !!req.params.auto;

  _buildAppID(hint, auto, function (appid, err) {
    if (err) {
      return res.status(400).send(err);
    } else {
      return res.json({
        appid: appid
      });
    }
  });
};

exports.getServerDetail = function (req, res) {
  var appid = req.params.appid;
  var appkey = req.params.appkey;

  Application.findOne({ 'appid': appid }).exec(function (err, application) {
    if (err) {
      return res.status(400).send(err);
    } else if (!application) {
      return res.status(404).send({
        message: 'Not exist Application.'
      });
    } else if (application) {
      if (application.appkey !== appkey) {
        return res.status(404).send({
          message: 'Not exist Application.'
        });
      } else {
        return res.json({
          server_host: application.server_host,
          api_port: application.server_port,
          xmpp_port: application.server_port + 2
        });
      }
    }
  });
};

/**
 * Application middleware
 */
exports.applicationByID = function (req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Application is invalid'
    });
  }

  Application.findById(id).populate('user', 'displayName').exec(function (err, application) {
    if (err) {
      return next(err);
    } else if (!application) {
      return res.status(404).send({
        message: 'No Application with that identifier has been found'
      });
    }
    req.application = application;
    next();
  });
};


/**
 * Update profile picture
 */
exports.changeProfilePicture = function (req, res) {
  var user = req.user;
  var application = req.application;
  var existingImageUrl;
  var multerConfig;


  if (useS3Storage) {
    multerConfig = {
      storage: multerS3({
        s3: s3,
        bucket: config.aws.s3.bucket,
        acl: 'public-read'
      })
    };
  } else {
    multerConfig = config.uploads.profile.image;
  }

  // Filtering to upload only images
  multerConfig.fileFilter = require(path.resolve('./config/lib/multer')).imageFileFilter;

  var upload = multer(multerConfig).single('newProfilePicture');

  if (user && application) {
    existingImageUrl = application.profileImageURL;
    uploadImage()
      .then(updateApp)
      .then(deleteOldImage)
      .then(function () {
        res.json(application);
      })
      .catch(function (err) {
        console.log(err);
        res.status(422).send(err);
      });
  } else {
    res.status(401).send({
      message: 'User is not signed in || application is not exist'
    });
  }

  function uploadImage() {
    return new Promise(function (resolve, reject) {
      upload(req, res, function (uploadError) {
        if (uploadError) {
          reject(errorHandler.getErrorMessage(uploadError));
        } else {
          resolve();
        }
      });
    });
  }

  function updateApp() {
    return new Promise(function (resolve, reject) {
      application.profileImageURL = config.uploads.storage === 's3' && config.aws.s3 ?
        req.file.location :
        '/' + req.file.path;
      application.save(function (err, theapp) {
        if (err) {
          reject(err);
        } else {
          application = theapp;
          resolve();
        }
      });
    });
  }

  function deleteOldImage() {
    return new Promise(function (resolve, reject) {
      if (existingImageUrl && existingImageUrl !== Application.schema.path('profileImageURL').defaultValue) {
        if (useS3Storage) {
          try {
            var { region, bucket, key } = amazonS3URI(existingImageUrl);
            var params = {
              Bucket: config.aws.s3.bucket,
              Key: key
            };

            s3.deleteObject(params, function (err) {
              if (err) {
                console.log('Error occurred while deleting old profile picture.');
                console.log('Check if you have sufficient permissions : ' + err);
              }

              resolve();
            });
          } catch (err) {
            console.warn(`${existingImageUrl} is not a valid S3 uri`);

            return resolve();
          }
        } else {
          fs.unlink(path.resolve('.' + existingImageUrl), function (unlinkError) {
            if (unlinkError) {

              // If file didn't exist, no need to reject promise
              if (unlinkError.code === 'ENOENT') {
                console.log('Removing profile image failed because file did not exist.');
                return resolve();
              }

              console.error(unlinkError);

              reject({
                message: 'Error occurred while deleting old profile picture'
              });
            } else {
              resolve();
            }
          });
        }
      } else {
        resolve();
      }
    });
  }
};
