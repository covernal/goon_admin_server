'use strict';

// Create the chat configuration
module.exports = function (app, io, socket) {

//   // Emit the status event when a new socket client is connected
//   io.emit('chatMessage', {
//     type: 'status',
//     text: 'Is now connected',
//     created: Date.now(),
//     profileImageURL: socket.request.user.profileImageURL,
//     username: socket.request.user.username
//   });

  socket.emit('channel', '?');

  // Send a chat messages to all connected sockets when a message is received
  socket.on('channel', function (message) {
    console.log('one socket accepted', message);
    socket.join(message);
  });

  // Emit the status event when a socket client is disconnected
  socket.on('disconnect', function () {
  });
};
