'use strict';

/**
 * Module dependencies
 */
var applicationsPolicy = require('../policies/applications.server.policy'),
  applications = require('../controllers/applications.server.controller');

module.exports = function (app) {
  // Applications Routes
  app.route('/api/applications').all(applicationsPolicy.isAllowed)
    .get(applications.list)
    .post(applications.create);

  app.route('/api/applications/:applicationId').all(applicationsPolicy.isAllowed)
    .get(applications.read)
    .put(applications.update)
    .delete(applications.delete);

  app.route('/api/applications/:applicationId/picture').all(applicationsPolicy.isAllowed)
    .post(applications.changeProfilePicture);

  app.route('/api/applications/:applicationId/newkey').all(applicationsPolicy.isAllowed)
    .put(applications.buildNewKey);

  app.route('/api/applications/getid/:hint/:auto?').all(applicationsPolicy.isAllowed)
    .get(applications.getNewAppID);

  app.route('/server/:appid/:appkey')
    .get(applications.getServerDetail);

  // Finish by binding the Application middleware
  app.param('applicationId', applications.applicationByID);
};
