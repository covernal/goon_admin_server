(function () {
  'use strict';

  angular
    .module('core')
    .run(routeFilter);

  routeFilter.$inject = ['$rootScope', '$state', 'Authentication'];

  function routeFilter($rootScope, $state, Authentication) {
    $rootScope.$on('$stateChangeStart', stateChangeStart);
    $rootScope.$on('$stateChangeSuccess', stateChangeSuccess);

    var disabledFilter = false;

    function stateChangeStart(event, toState, toParams, fromState, fromParams) {
      // Check authentication before changing state
      if (disabledFilter) return;

      console.log("toState.name.startsWith('password.forgot') = ", toState.name.startsWith('password.'));
      if (!!!Authentication.user && !(
        toState.name === 'authentication.signin' ||
        toState.name === 'authentication.signup' ||
        toState.name.startsWith('password.')
        )) {
        event.preventDefault();
        disabledFilter = true;
        $state.go('authentication.signin').then(function () {
          disabledFilter = false;
        });
        return;
      } else {
        if (toState.name === 'home' || toState.name === '') {
          event.preventDefault();
          disabledFilter = true;
          $state.go('applications.list').then(function () {
            disabledFilter = false;
          });
        }
      }
    }

    function stateChangeSuccess(event, toState, toParams, fromState, fromParams) {
      // Record previous state
      // if (!!!Authentication.user && !(
      //   $state.is('authentication.signin') ||
      //   $state.is('authentication.signup') ||
      //   $state.is('password.forgot')
      //   )) {
      //   $state.go('authentication.signin').then(function () {
      //   });
      //   return;
      // } else {
      //   if ($state.is('home')) {
      //     $state.go('applications.list').then(function () {
      //     });
      //   }
      // }

      disabledFilter = false;

      storePreviousState(fromState, fromParams);
    }

    // Store previous state
    function storePreviousState(state, params) {
      // only store this state if it shouldn't be ignored
      if (!state.data || !state.data.ignoreState) {
        $state.previous = {
          state: state,
          params: params,
          href: $state.href(state, params)
        };
      }
    }
  }
}());
